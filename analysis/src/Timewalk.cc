/* 
 * File:   Timewalk.h
 * Author: JW
 *
 * Created on 22. July 2014,
 */

#include "Timewalk.h"

void Timewalk::init(const TBCore* core)
{
	int iden;
	int iden2;
	int maxToT;
		
	for(auto dut: core->usedDUT)
	{
		iden = dut->iden;
		maxToT = dut->getMaxTot();	//assuming all DUTs have the same maxToT

		for(auto dut2: core->usedDUT)
		{
			iden2 = dut2->iden;

			if(iden2 == iden)
			{
				continue;
			}

			auto p1 = std::make_pair(iden, iden2);
			auto pD = std::make_pair(dut, dut2);
			h_delayHistMatched[p1] = new TH1F("", ";All Hits Lvl1", maxToT+1, -.5, maxToT+.5);
			h_timewalkHistMatched[pD] = new TH1F("", ";Matched Hits Lvl1", 2*maxToT-1, -(maxToT+.5), maxToT+.5);
			h_DtHist[pD] = new TH1F("Delta_t","Delta t vs trigger number", 2*maxToT-1, -(maxToT+.5), maxToT+.5);
		}
	}
}

void Timewalk::event(const TBCore* core, const TBEvent* event)
{
	
	DUT* dut = event->dut;
	int iden = event->iden;		//This is the DUT id

	int trigNo, trackNo;
	
	if(event->fTracks != kGood) 
	{
		return;	//analyse only events that contain good tracks
	}
	
	for(auto tbtrack: event->tracks)
	{
		trigNo = tbtrack->trig;
		trackNo = tbtrack->trackNum;
		auto id = std::make_pair(trigNo, trackNo);

		if(tbtrack->fMatchedHit == kGood)
		{
			//store Lv1 of matched hit for this id pair
			m_L1collection[id][dut] = tbtrack->matchedHit->getLv1();
		}
		
	}

}

void Timewalk::finalize(const TBCore* core)
{

	core->output->processName = Timewalk::name;

	char* histoTitle = new char[500];
	
	int ref_lv1 = 0;
	int last_trig = -1;
	int trig = -1;
	int dut0_id, dut1_id;
	
	std::map<DUT*, int> L1map;
	std::map<DUT*, int>::iterator L1map_it;
	
	//loop over all tracks, i.e. all keys in map m_L1collection
	std::map<std::pair<int, int>, std::map<DUT*, int> >::iterator coll_it;
	
	for(coll_it = m_L1collection.begin(); coll_it!= m_L1collection.end(); ++coll_it)			//loop through all tracks
	{

		trig = coll_it->first.first;		//get current trigNum
		if(last_trig == -1) last_trig = trig;	//first trigNum in the map
		
		L1map = coll_it->second;
		L1map_it = L1map.begin();

		//need at least two DUTs with a hit
		if(L1map.size() <= 1) continue;
		
		
		for(L1map_it = L1map.begin(); L1map_it != L1map.end(); ++L1map_it)		//loop through L1collection per track
		{
			//store Lv1 for reference DUT
			ref_lv1 = (*L1map_it).second;

			for(auto L1_DUTs : L1map)							//loop over all DUTs for this track
			{
				//check for different DUTs
				if(L1_DUTs.first != (*L1map_it).first)
				{
					auto p1 = make_pair((*L1map_it).first, L1_DUTs.first);
					if(m_averageDel.find(p1) == m_averageDel.end()) m_averageDel[p1] = 0;	//no sum exists for this pair of DUTs, initialize entry before summing
					
					m_averageDel[p1] += (ref_lv1 - L1_DUTs.second);	//add up L1 differences for this pair of DUTs over all tracks for this trigNum
					m_averageDt[p1].push_back(ref_lv1 - L1_DUTs.second);
					h_DtHist[p1]->Fill(ref_lv1 - L1_DUTs.second);
					m_trackCount[p1]++;
				}
			} 
			
		}
		
	}
	
	for(auto & avg_it : m_averageDel)		//calculate average delay for each pair of DUTs
	{
		avg_it.second = round((float)avg_it.second/m_trackCount[avg_it.first]);
	}

	for(auto del_it : m_averageDt)		//histogram timewalk = Delta_t - <delay>
	{
		auto pD = del_it.first;			//pair of DUT*s
		for(auto vec_it : del_it.second)
		{
			h_timewalkHistMatched[pD]->Fill(vec_it-m_averageDel[pD]);
		}
	}
	

	for(auto DUT0: core->usedDUT)		//loop over all DUTs again to plot and save histograms
	{
		for(auto DUT1: core->usedDUT)
		{
			dut0_id = DUT0->iden;
			dut1_id = DUT1->iden;

			auto pI = std::make_pair(dut0_id, dut1_id);
			auto pD = std::make_pair(DUT0, DUT1);
			
			if(dut0_id == dut1_id) continue;
			
			// set up cuts
			core->output->cuts = "only events with good tracks";
			core->output->currentPreprocessCuts = "";
			// -----------
			
			std::sprintf(histoTitle, "Lv1 difference DUT %i wrt DUT %i", dut0_id, dut1_id);
			h_DtHist[pD]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "Dt_dut_%i-dut_%i", dut0_id, dut1_id);
			h_DtHist[pD]->SetName(histoTitle);
			std::sprintf(histoTitle, "#Delta Lv1 [BC]");
			h_DtHist[pD]->GetXaxis()->SetTitle(histoTitle);
			core->output->drawAndSave(h_DtHist[pD], "", "e");
			
			std::sprintf(histoTitle, "Timewalk DUT %i wrt DUT %i", dut0_id, dut1_id);
			h_timewalkHistMatched[pD]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "timewalk_dut_%i-dut_%i", dut0_id, dut1_id);
			h_timewalkHistMatched[pD]->SetName(histoTitle);
			std::sprintf(histoTitle, "Timewalk [BC]");
			h_timewalkHistMatched[pD]->GetXaxis()->SetTitle(histoTitle);
			core->output->drawAndSave(h_timewalkHistMatched[pD], "", "e");

			delete h_DtHist[pD];
			delete h_timewalkHistMatched[pD];
		}
	}

	h_DtHist.clear();
	h_timewalkHistMatched.clear();
	delete[] histoTitle;
}

