/* 
 * File:   EfficiencyVsRun.cc
 * Author: daniel
 * 
 * Created on 6. März 2014, 23:49
 */

#include "EfficiencyVsRun.h"

void EfficiencyVsRun::init(const TBCore* core)
{
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;

		for(auto currentRun: core->tbconfig->rawDataRuns)
		{
			EfficiencyVsRun::ntracks[dut->iden][currentRun] = 0;
			EfficiencyVsRun::nhits[dut->iden][currentRun] = 0;
			EfficiencyVsRun::nmatched[dut->iden][currentRun] = 0;
		}
		
		std::string param = core->getParam(iden, EfficiencyVsRun::name, "effPlotMinY");
		if(param.compare("") != 0)
		{
			EfficiencyVsRun::effPlotMinY[iden] = std::stod(param);
		}
		else // default value
		{
			EfficiencyVsRun::effPlotMinY[iden] = 0.9;
		}
	}
}

void EfficiencyVsRun::event(const TBCore* core, const TBEvent* event)
{
	DUT* dut = event->dut;
	int iden = event->iden;
	int currentRun = core->currentRun;
	
	if(event->fTracks != kGood)
	{
		return;
	}

	for(auto tbtrack: event->tracks)
	{
		if(tbtrack->fTrackMaskedRegion == kGood)
		{
			continue;
		}
		
		EfficiencyVsRun::ntracks[iden][currentRun]++;

		if(event->fHits == kGood)
		{
			EfficiencyVsRun::nhits[iden][currentRun]++;
		}
		
		if(tbtrack->fMatchedCluster != kGood)
		{
			continue;
		}
		
		EfficiencyVsRun::nmatched[iden][currentRun]++;
	}
}

void EfficiencyVsRun::finalize(const TBCore* core)
{
	core->output->processName = EfficiencyVsRun::name;
	
	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		
		// set up cuts
		core->output->cuts = "good matched tracks";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		int nRuns = EfficiencyVsRun::ntracks[iden].size();
		double* runs = new double[nRuns];
		double* effall = new double[nRuns];
		double* e_effall = new double[nRuns];
		double* effmatched = new double[nRuns];
		double* e_effmatched = new double[nRuns];

		int minRunNum = core->tbconfig->rawDataRuns.at(0);
		int maxRunNum = core->tbconfig->rawDataRuns.at(nRuns-1);
		
		EfficiencyVsRun::histo_EffAllVsRun[iden] = new TH1D("", ";Run Number;(All) Efficiency %", maxRunNum - minRunNum + 1, minRunNum, maxRunNum + 1);
		EfficiencyVsRun::histo_EffMatchedVsRun[iden] = new TH1D("", ";Run Number;(Matched) Efficiency %", maxRunNum - minRunNum + 1, minRunNum, maxRunNum + 1);
		
		for(int irun = 0; irun < nRuns; irun++)
		{
			int run = core->tbconfig->rawDataRuns.at(irun);
			runs[irun] = (double) run;
			double trk = (double) (EfficiencyVsRun::ntracks[iden][run]);
			double hit = (double) (EfficiencyVsRun::nhits[iden][run]);
			double matched = (double) (EfficiencyVsRun::nmatched[iden][run]);
			
			if(trk > 0)
			{
				effall[irun] = hit / trk;
				if(effall[irun] < 1.0)
				{
					e_effall[irun] = std::sqrt(effall[irun] * (1. - effall[irun]) / trk);
				}
				else
				{
					e_effall[irun] = 0.0;
				}

				effmatched[irun] = matched / trk;
				if(effmatched[irun] < 1.0)
				{
					e_effmatched[irun] = std::sqrt(effmatched[irun] * (1. - effmatched[irun]) / trk);
				}
				else
				{
					e_effmatched[irun] = 0.0;
				}

			}
			else
			{
				effall[irun] = 0.0;
				e_effall[irun] = 0.0;

				effmatched[irun] = 0.0;
				e_effmatched[irun] = 0.0;
			}
			EfficiencyVsRun::histo_EffAllVsRun[dut->iden]->SetBinContent(run - minRunNum + 1, effall[irun]);
			EfficiencyVsRun::histo_EffAllVsRun[dut->iden]->SetBinError(run - minRunNum + 1, e_effall[irun]);
			EfficiencyVsRun::histo_EffMatchedVsRun[dut->iden]->SetBinContent(run - minRunNum + 1, effmatched[irun]);
			EfficiencyVsRun::histo_EffMatchedVsRun[dut->iden]->SetBinError(run - minRunNum + 1, e_effmatched[irun]);
		}
		
		std::sprintf(histoTitle, "Efficiency All Tracks Vs Run DUT %i", iden);
		EfficiencyVsRun::histo_EffAllVsRun[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "effAllVsRun_dut_%i", iden);
		EfficiencyVsRun::histo_EffAllVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(EfficiencyVsRun::histo_EffAllVsRun[iden], "", "m");
		
		std::sprintf(histoTitle, "Efficiency Matched Tracks Vs Run DUT %i", iden);
		EfficiencyVsRun::histo_EffMatchedVsRun[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "effMatchedVsRun_dut_%i", iden);
		EfficiencyVsRun::histo_EffMatchedVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(EfficiencyVsRun::histo_EffMatchedVsRun[iden], "", "m");
		
		
		EfficiencyVsRun::h_EffAllVsRun[iden] = new TGraphErrors(nRuns, runs, effall, 0, e_effall);
		EfficiencyVsRun::h_EffMatchedVsRun[iden] = new TGraphErrors(nRuns, runs, effmatched, 0, e_effmatched);
		
		std::sprintf(histoTitle, "Efficiency All Tracks Vs Run DUT %i", iden);
		EfficiencyVsRun::h_EffAllVsRun[iden]->SetTitle(histoTitle);
		EfficiencyVsRun::h_EffAllVsRun[iden]->GetXaxis()->SetTitle("Run number");
		EfficiencyVsRun::h_EffAllVsRun[iden]->GetYaxis()->SetTitle("Efficiency");
		EfficiencyVsRun::h_EffAllVsRun[iden]->GetYaxis()->SetRangeUser(EfficiencyVsRun::effPlotMinY[iden], 1.01);
		std::sprintf(histoTitle, "effAllVsRunGraphError_dut_%i", iden);
		EfficiencyVsRun::h_EffAllVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(EfficiencyVsRun::h_EffAllVsRun[iden], "AP", "");
		
		std::sprintf(histoTitle, "Efficiency Matched Tracks Vs Run DUT %i", iden);
		EfficiencyVsRun::h_EffMatchedVsRun[iden]->SetTitle(histoTitle);
		EfficiencyVsRun::h_EffMatchedVsRun[iden]->GetXaxis()->SetTitle("Run number");
		EfficiencyVsRun::h_EffMatchedVsRun[iden]->GetYaxis()->SetTitle("Efficiency");
		EfficiencyVsRun::h_EffMatchedVsRun[iden]->GetYaxis()->SetRangeUser(EfficiencyVsRun::effPlotMinY[iden], 1.01);
		std::sprintf(histoTitle, "effMatchedVsRunGraphError_dut_%i", iden);
		EfficiencyVsRun::h_EffMatchedVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(EfficiencyVsRun::h_EffMatchedVsRun[iden], "AP", "");
		
		delete [] runs;
		delete [] effall;
		delete [] e_effall;
		delete [] effmatched;
		delete [] e_effmatched;
		
		delete EfficiencyVsRun::histo_EffAllVsRun[iden];
		delete EfficiencyVsRun::histo_EffMatchedVsRun[iden];
		delete EfficiencyVsRun::h_EffAllVsRun[iden];
		delete EfficiencyVsRun::h_EffMatchedVsRun[iden];
	}
	EfficiencyVsRun::histo_EffAllVsRun.clear();
	EfficiencyVsRun::histo_EffMatchedVsRun.clear();
	EfficiencyVsRun::h_EffAllVsRun.clear();
	EfficiencyVsRun::h_EffMatchedVsRun.clear();
	
	delete [] histoTitle;
}
