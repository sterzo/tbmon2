> Thu May  2 11:38:43 CEST 2019 - tested on CentOS Linux release 7.6.1810 (Core) with gcc 4.8.5 and ROOT 6.16.00

## Installation

### Prerequisites
 * have ROOT installed

## Compile libconfig: follow the steps below

`cd lib/libconfig-1.4.9/`

`./configure`

`make`

## Compile tbmon2

`cd ../../`

`make`

## Run tbmon2 for the first time

`./tbmon2 /absolute/path/to/output/folder/`

* Nota Bene:
- please be sure you add the last `/` each time and the output folder does not yet exist 
- edit the `mainConfig.cfg` and `analysisConfig.cfg` in the folder "config" in your output folder
- run again `./tbmon2 /absolute/path/to/output/folder/`

## Configuration

* All configuration is done in mainConfig.cfg and analysisConfig.cfg. Both can be found in "config" in your output folder. They configure the DUT setup and the analyses, respectively.

## FAQ

* How do I open the config/log in the root output?
- Right click on it and choose "Print". The config will then be printed in your console.

* The labels in the generated plots are off/not where they should be.
- Try using `"useAtlasStyle = false;"`. The plots are optimised for plain style.

* I get an error like 
> settings.useDUT.[0].matchRadius Does not exist in config/DUT/FE-I4.cfg
 although the parameter is defined.
- Probably the parameter type is wrong. Try e.g. `1.0` instead of `1`.

* ROOT libs are not found.
- add
`export ROOTSYS=/PATH/TO/ROOT`
`export PATH=$ROOTSYS/bin:$PATH`
`export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH`
in ${HOME}/.bashrc or the configuration file of your shell

* I get the following error :

> Parse error at ~/tbmon2/test1/config/mainConfig.cfg:48 - duplicate setting name 
- All chosen analysis packages have to be run from single 1 line

e.g. add:

`useAnalyses = ["BeamProfile", "ClusterChecker"];`

and not:

`useAnalyses = ["BeamProfile"];`

`useAnalyses = ["ClusterChecker"];`
