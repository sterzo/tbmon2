/* 
 * File:   TBAnalysis.h
 * Author: Daniel Kalin
 *
 * Created on 26. Januar 2014
 */

#ifndef TBANALYSIS_H
#define	TBANALYSIS_H

class TBAnalysis;
#include "TBEvent.h"
#include "TBCore.h"
#include "TBDut.h"
#include <map>
#include <iostream>
//#include "tbutils.h"
#include <sstream>

#include <TH2D.h>
#include <TH1D.h>

//#include "clusters.h"

#include <string>

#include <TCanvas.h>
#include <TH3D.h>
#include <TLegend.h>
#include <TProfile2D.h>
#include <TVector2.h>

#include <cmath>

#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>

#include <assert.h>

#include <TGraphAsymmErrors.h>
#include <TMath.h>
#include <TPaletteAxis.h>
#include <TPaveStats.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TGraphErrors.h>

#include <TF1.h>

#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <TProfile.h>

//standard
#include <algorithm>

//root
#include <TGraph2D.h>
#include <TPad.h>
#include <THStack.h>

class TBAnalysis
{
public:
	typedef int IDEN;
	
	std::string name;

	TBAnalysis()
	{ 
		TBAnalysis::name = "Unknown";
	};
	//Stuff to be done before anything else
	virtual void init(const TBCore* core) = 0;
	//How to process an event
	virtual void event(const TBCore* core, const TBEvent* event) = 0;
	//Stuff to be done after all events have been processed
	//virtual void finalize(const TbConfig &config) = 0;
	virtual void finalize(const TBCore* core) = 0;
	//Stuff to be done at the beginning of each run
	virtual void initRun(const TBCore* core){;}
	//Stuff to be done at the end of each run
	virtual void finalizeRun(const TBCore* core){;}
};

typedef TBAnalysis* createTBA_t();
typedef void destroyTBA_t(TBAnalysis*);

#endif	/* TBANALYSIS_H */

